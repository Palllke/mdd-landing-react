export const canDo = [
  {
    img: 'img/can-do/can-do-1.svg',
    text: 'Add new pages'
  },
  {
    img: 'img/can-do/can-do-2.svg',
    text: 'Update existing pages'
  },
  {
    img: 'img/can-do/can-do-3.svg',
    text: 'Implement new features'
  },
  {
    img: 'img/can-do/can-do-4.svg',
    text: 'Mobile adaptation'
  },
  {
    img: 'img/can-do/can-do-5.svg',
    text: 'Update part of design'
  },
  {
    img: 'img/can-do/can-do-6.svg',
    text: 'Create new design and implement it'
  },
  {
    img: 'img/can-do/can-do-7.svg',
    text: 'Move to another CMS'
  },
  {
    img: 'img/can-do/can-do-8.svg',
    text: 'Move to another hosting'
  },
  {
    img: 'img/can-do/can-do-9.svg',
    text: 'Search Engine Optimization'
  },
  {
    img: 'img/can-do/can-do-10.svg',
    text: 'Integrate with third-party services'
  }
];


export const trusted = [
  {
    id: 'trusted-1',
    img: 'img/trusted/trusted-1.svg'
  },
  {
    id: 'trusted-2',
    img: 'img/trusted/trusted-2.svg'
  },
  {
    id: 'trusted-3',
    img: 'img/trusted/trusted-3.svg'
  },
  {
    id: 'trusted-4',
    img: 'img/trusted/trusted-4.svg'
  },
  {
    id: 'trusted-5',
    img: 'img/trusted/trusted-5.svg'
  },
  {
    id: 'trusted-6',
    img: 'img/trusted/trusted-6.svg'
  },
  {
    id: 'trusted-7',
    img: 'img/trusted/trusted-7.svg'
  },
  {
    id: 'trusted-8',
    img: 'img/trusted/trusted-8.svg'
  },
  {
    id: 'trusted-9',
    img: 'img/trusted/trusted-9.svg'
  }
];

export const howWorks = [
  {
    id: 1,
    title: '1. Application',
    text: 'Fill form and send an application.',
    img: 'img/how-works/howWorks-1.jpg',
    imgAlt: 'Send an application'
  },
  {
    id: 2,
    title: '2. Evaluation',
    text: 'Your website evaluation. It is for free.',
    img: 'img/how-works/howWorks-2.jpg',
    imgAlt: 'Your website evaluation'
  },
  {
    id: 3,
    title: '3. Discussion',
    text: 'Discussion of your business needs and task estimates.',
    img: 'img/how-works/howWorks-3.jpg',
    imgAlt: 'Your business needs discussion'
  },
  {
    id: 4,
    title: '4. Contract',
    text: 'Fixing scope of tasks, contract signing.',
    img: 'img/how-works/howWorks-4.jpg',
    imgAlt: 'Contract signing'
  },
  {
    id: 5,
    title: '5. Payment',
    text: '50% prepayment in order to start development.',
    img: 'img/how-works/howWorks-5.jpg',
    imgAlt: 'Payment'
  },
  {
    id: 6,
    title: '6. Implementation',
    text: 'All agreed task be implemented.',
    img: 'img/how-works/howWorks-6.jpg',
    imgAlt: 'Changes implementation'
  },
  {
    id: 7,
    title: '7. Testing',
    text: 'All changes be tested that everything works as expected.',
    img: 'img/how-works/howWorks-7.jpg',
    imgAlt: 'Testing'
  },
  {
    id: 8,
    title: '8. Production',
    text: 'Implemented and tested changes be added to your production.',
    img: 'img/how-works/howWorks-8.jpg',
    imgAlt: 'Update production'
  },
  {
    id: 9,
    title: '9. Gift',
    text: 'We love our clients and present them with secret gift.',
    img: 'img/how-works/howWorks-9.jpg',
    imgAlt: 'Gift for you'
  }
];

export const technology1 = [
  {
    id: 1,
    img: 'img/technology/technology-1.svg'
  },
  {
    id: 2,
    img: 'img/technology/technology-2.svg'
  },
  {
    id: 3,
    img: 'img/technology/technology-3.svg'
  },
  {
    id: 4,
    img: 'img/technology/technology-4.svg'
  },
  {
    id: 5,
    img: 'img/technology/technology-5.svg'
  },
  {
    id: 6,
    img: 'img/technology/technology-6.svg'
  },
  {
    id: 7,
    img: 'img/technology/technology-7.svg'
  },
  {
    id: 8,
    img: 'img/technology/technology-8.svg'
  },
  {
    id: 9,
    img: 'img/technology/technology-9.svg'
  },
  {
    id: 10,
    img: 'img/technology/technology-10.svg'
  },
  {
    id: 11,
    img: 'img/technology/technology-11.svg'
  },
  {
    id: 12,
    img: 'img/technology/technology-12.svg'
  },
  {
    id: 13,
    img: 'img/technology/technology-13.svg'
  },
  {
    id: 14,
    img: 'img/technology/technology-14.svg'
  }
];

export const technology2 = [
  {
    id: 15,
    img: 'img/technology/technology-14.svg'
  },
  {
    id: 16,
    img: 'img/technology/technology-16.svg'
  },
  {
    id: 17,
    img: 'img/technology/technology-17.svg'
  },
  {
    id: 18,
    img: 'img/technology/technology-18.svg'
  },
  {
    id: 19,
    img: 'img/technology/technology-19.svg'
  }
];

export const technology3 = [
  {
    id: 20,
    img: 'img/technology/technology-20.svg'
  },
  {
    id: 21,
    img: 'img/technology/technology-21.svg'
  },
  {
    id: 22,
    img: 'img/technology/technology-22.svg'
  },
  {
    id: 23,
    img: 'img/technology/technology-23.svg'
  },
  {
    id: 24,
    img: 'img/technology/technology-24.svg'
  },
  {
    id: 25,
    img: 'img/technology/technology-25.svg'
  },
  {
    id: 26,
    img: 'img/technology/technology-26.svg'
  }
];


export const faq = [
  {
    id: 1,
    title: 'How long does it take to complete a project?',
    text: 'We understand that time is money, so we do our best to finish each project as soon as possible. Each project is one of a kind and can take unique amount of time. Let us have a look first to provide you with exact numbers.'
  },
  {
    id: 2,
    title: 'Will my website work during development?',
    text: 'Sure. Development will be done separately. Your website will be updated only after finishing all works, changes are tested by us and verified by you.'
  },
  {
    id: 3,
    title: 'Does data remain after refining?',
    text: 'We very careful with data and take care that all data remain. Moreover we do backup before doing refining for website.'
  },
  {
    id: 4,
    title: 'What if part of current functional would be broken after upgrading website?',
    text: 'First of all website will be backuped before upgrading. So, changes can be reverted at any time. It is possible to have conflict between old and new functional. But we definitely take care about it.'
  },
  {
    id: 5,
    title: 'Which CMS are you using?',
    text: 'We are using all popular CMS such as Wordpress, OpenCart, MODX, Magento, Joomla, Drupal, TYPO3. If your CMS is not listed it is not a problem for us, we can take care about it.'
  },
  {
    id: 6,
    title: 'How much does it cost?',
    text: 'It depends on scope of work to be done. We propose affordable price for our customers. It means you don\'t have to worry about budget.'
  },
  {
    id: 7,
    title: 'I need my website to be refined asap. What can you offer?',
    text: 'We need to know your deadlines first if you have them. We will try to work out a very special offer for you.'
  },
  {
    id: 8,
    title: 'What are the payment terms?',
    text: '50% prepayment in order to start development and rest of 50% after changes demonstration. We can consider other options for our reliable partners.'
  },
  {
    id: 9,
    title: 'Do you need designs for tablet/mobile?',
    text: 'It is nice to have designs in order to site look exactly as you want to. In other case we will do our best to make your site look awesome on tab and mobile devices.'
  },
  {
    id: 10,
    title: 'Can you just do development by my designs?',
    text: 'Yes, certainly can and we will do it!'
  }
];

export const feedback = [
  {
    id: 1,
    text: 'They are professionals, competent, creative, experienced. Landing page was launched in a very short time, but it did not affect the quality. I\'m glad I turned here. Our cooperation was productive.'
  },
  {
    id: 2,
    text: 'Many thanks to the MDD company for the operational refinement of our two landing pages. We are delighted with the unusual new design and will definitely turn again! I pleased with the result. Very responsible and creative guys.'
  },
  {
    id: 3,
    text: 'A couple of months ago, used the services of the company MDD, were very pleased! Our site turned out beautiful and comfortable. The constant technical support of the company\'s employees is of great importance, for which we are especially grateful to them!'
  },
  {
    id: 4,
    text: 'Thank you guys for the website refinement! Always in touch, made concessions! Working with them is very comfortable! The deadlines were delayed a bit, this is probably the only negative thing, but whoever did the site for himself understands that it is impossible to foresee everything and many tasks change in the process of work. After the end of the main work, support the site, make minor improvements, etc. In general, they will not leave you after the main work.'
  },
  {
    id: 5,
    text: 'The specialists of MD Development Group fulfilled all the tasks assigned to them, taking into account all our wishes, and not only those that were initially agreed. E-commerce is developing actively, you need to constantly refine the site. Now that we have such a wonderful partner, we are not afraid of change and are ready to move ahead with confidence.  We express our sincere gratitude to the staff of the company MD Development Group for their professionalism, efficiency and patience, attention to detail and readiness to help at any moment!'
  }
];


export const reasons = [
  {
    id: 1,
    text: 'We have 5+ years of experience'
  },
  {
    id: 2,
    text: 'We are result oriented'
  },
  {
    id: 3,
    text: 'We finish projects ASAP'
  },
  {
    id: 4,
    text: 'We adhere to complex approach to business'
  },
  {
    id: 5,
    text: 'We are recommended by past and current customers'
  }
];
