import React, { Component } from 'react';


export default function FormWrapper(ChildComponentClass) {


  return class Container extends Component {
    state = {
      span: null,
      spanText: null,
      emailClasses: null,
      textClasses: null,
      textError: true,
      emailError: true,
      name: '',
      email: '',
      text: '',
      site: ''
    };

    handleUserInput = (evt) =>{
     this.setState({
       [evt.target.name]: evt.target.value
     })
    };

    validation = (elem, type = '') => {
      if (elem.trim() === '') {
        if( type === 'email') {
          this.setState({
            span: <span className="error email invalid"  style={{opacity: 1}}>The field can not be empty!</span>,
            emailClasses: 'invalid',
            emailError: true
          });
        }
        if (type === 'text') {
          this.setState({
            spanText: <span className="error email invalid"  style={{opacity: 1}}>The field can not be empty!</span>,
            textClasses: 'invalid',
            textError: true
          });
        }
        return false;
      }
      if (type === 'email') {
        const RE_EMAIL = /^[\w]{1}[\w-.]*@[\w-]+\.[a-z]{2,4}(\.[a-z]{2,4})?$/i;
        if(RE_EMAIL.test(elem)) {
          this.setState({
            span: null,
            emailClasses: 'not-empty valid',
            emailError: false
          });
        }
        if (!RE_EMAIL.test(elem)) {
          this.setState({
            span: <span className="error email invalid"  style={{opacity: 1}}>Incorrect email</span>,
            emailClasses: 'not-empty invalid',
            emailError: true
          });
        }
        return false;
      }
      this.setState({
        spanText: null,
        textClasses: 'valid not-empty',
        textError: false
      });
    };


    blur = (evt) => {
      this.validation(evt.target.value, evt.target.name);
    };


    submit = (evt, ...elem) => {
      evt.preventDefault();
      elem.forEach((elem) => {
        this.validation(this.state[elem], elem)
      });
      const noValidArray = elem.filter((elem) => {
        return this.state[elem + 'Error']
      });
      if( noValidArray.length === 0 ) {
        console.log('Form was Send');
        fetch('../mailer.php', {
          method: 'post',
          headers: {
            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
          },
          body: `foo=bar&lorem=ipsum`
        })
          .then(response => console.log(response))
      }
    };


    render() {
      return <ChildComponentClass
        {...this.props}
        {...this.state}
        blur={this.blur}
        handleUserInput={this.handleUserInput}
        submit={this.submit}
      />;
    }
  }
}
