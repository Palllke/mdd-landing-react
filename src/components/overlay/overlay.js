import React from 'react';

export default function Overlay() {
  return(
    <div className="overlay">
      <div className='modal' id='modal'>
        <div className='content'>
          <p />
          <button className='button-black-modal close-modal' data-modal="#modal">Close</button>
        </div>
      </div>
    </div>
  )
}
