import React from 'react';

export default function Cookies({cookies, handleClose}) {

  return (
    <div className={`cookies-wrapper ${cookies&&'active'}`}>
      <button id="cookies__close" className="cookies__close" onClick={handleClose} />

      <div className="cookies__text-block">
        <h4>This Website Uses Cookies:</h4>
        <p>We do not capture your personal data or transfer it to anyone else, or use any third-party cookies. You can
          disable the use of cookies from your browser settings. By closing this message and continuing to use our
          website you consent to our use of cookies.</p>
      </div>
    </div>
  )
}
