import React from 'react';

export default function Footer() {
  return (
    <footer className="footer">
      <p className="footer-copyright">&copy; MD Development Group. 2019</p>
    </footer>
  )
}
