import React from 'react';

import Logo from './img/logo.svg'

function Header() {
  return (
    <header>
      <div className="header-logo">
        <a href="#index">
            <span>
                <img src={Logo} alt="yes"/>
            </span>
        </a>
      </div>
    </header>
  )
}

export default Header;
