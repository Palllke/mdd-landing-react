import React from 'react';


export default function Loader({loading}) {
  return (
    <div id="loading" className={loading ? 'initial' : null}>
      <div className="loader">
        <p>Welcome to MD Development Group</p>
      </div>
    </div>
  )
}
