import React from 'react';
import AnchorLink from 'react-anchor-link-smooth-scroll';

import TrustedItem from './trusted-item';
import { trusted } from '../../../data';

export default function Trusted() {
  const trustedItems = trusted.map((elem)=> <TrustedItem key={elem.id} img={elem.img} id={elem.id}/>);
  return (
    <div id="trusted" className="trusted">
      <div className="trusted-wrapper">
        <h2 className="section-title section-title-right"><span>We are trusted</span></h2>
        <div className="trusted-item-wrapper">
          {trustedItems}

        </div>
      </div>
      <AnchorLink offset='100' href="#howWorks" className="down-button"><span>How it works</span></AnchorLink>
    </div>
  )
}
