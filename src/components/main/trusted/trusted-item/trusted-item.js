import React from 'react';

export default function TrustedItem({img, id}) {
  return (
    <div className={`trusted-item ${id}`} data-aos="fade-right"
         data-aos-anchor-placement="bottom-bottom">
      <img src={img} alt="trusted logo"/>
    </div>
  )
}
