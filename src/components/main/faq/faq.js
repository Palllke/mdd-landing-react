import React, { useState } from 'react';
import AnchorLink from 'react-anchor-link-smooth-scroll';

import FaqItem from './faq-item';

import { faq } from '../../../data';

export default function Faq() {
  const [show, changeShow] = useState(null);
  function handleQuestion(id) {
    if(id === show){
      changeShow(null);
      return false;
    }
    changeShow(id)
  }

  const faqItems = faq.map((elem) => <FaqItem
    key={elem.id}
    title={elem.title}
    text={elem.text}
    show={show}
    handleQuestion={handleQuestion}
    id={elem.id}
  />);
  return (
    <div className="faq" id="faq">
      <div className="faq-wrapper">
        <h2 className="section-title"><span>FAQ</span></h2>
        <div className="faq-item-wrapper">
          {faqItems}
        </div>
      </div>
      <AnchorLink offset='100' href="#feedback" className="down-button down-button-left"><span>Feedback</span></AnchorLink>
    </div>
  )
}



