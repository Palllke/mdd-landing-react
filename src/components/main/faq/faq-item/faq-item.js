import React from 'react';
import arrow from './faq.svg'

export default function FaqItem({title, text, show, handleQuestion, id}) {
  return (
    <div className="faq-item" data-aos="fade-left">
      <p
        className={`faq-question ${show ===id &&'faq-question-black'}`}
        onClick={() => handleQuestion(id)}
      >
        <span><img src={arrow} alt=""/></span>
        {title}
      </p>
      <p className={`faq-answer ${show === id &&'show'}`}>{text}</p>
    </div>
  )
}
