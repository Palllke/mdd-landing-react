import React from 'react';
import MainTop from './main-top';
import CanDo from './can-do';
import Trusted from './trusted';
import HowWorks from './how-works';
import EvaluateWebsite from './evaluate-website';
import Technology from './technology';
import Reason from './reason';
import AppFree from './app-free';
import Faq from './faq';
import Feedback from './feedback';
import Question from './question';

export default function Main() {
  return(
    <main>
      <MainTop />
      <CanDo />
      <Trusted />
      <HowWorks />
      <EvaluateWebsite />
      <Technology />
      <Reason />
      <AppFree />
      <Faq />
      <Feedback />
      <Question />
    </main>
  )
}
