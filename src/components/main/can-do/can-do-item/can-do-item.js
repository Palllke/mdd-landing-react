import React from 'react';

export default function CanDoItem({img, text}) {
  return(
    <div className="canDo-item" data-aos="fade-up">
      <div className="canDo-item__image">
        <img src={img} alt={text}/>
      </div>
      <p>{text}</p>
    </div>
  )
}
