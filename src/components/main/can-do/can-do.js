import React from 'react';
import AnchorLink from 'react-anchor-link-smooth-scroll';

import { canDo } from '../../../data';
import CanDoItem from './can-do-item';

export default function CanDo() {

  return (
    <div id="canDo" className="canDo">
      <div className="canDo-wrapper">
        <h2 className="section-title"><span>What we can do for you</span></h2>
        <div className="canDo-item-wrapper">
          <div className="flex-row canDo-wrapper-1">
            <CanDoItem img={canDo[0].img} text={canDo[0].text}/>
            <CanDoItem img={canDo[1].img} text={canDo[1].text}/>
          </div>
          <div className="flex-row canDo-wrapper-2">
            <CanDoItem img={canDo[2].img} text={canDo[2].text}/>

            <CanDoItem img={canDo[3].img} text={canDo[3].text}/>

            <CanDoItem img={canDo[4].img} text={canDo[4].text}/>
          </div>
          <div className="flex-row canDo-wrapper-3">
            <CanDoItem img={canDo[5].img} text={canDo[5].text}/>

            <CanDoItem img={canDo[6].img} text={canDo[6].text}/>

            <CanDoItem img={canDo[7].img} text={canDo[7].text}/>
          </div>
          <div className="flex-row canDo-wrapper-4">
            <CanDoItem img={canDo[8].img} text={canDo[8].text}/>

            <CanDoItem img={canDo[9].img} text={canDo[9].text}/>
          </div>
        </div>
      </div>

      <AnchorLink offset='100' href="#trusted" className="down-button down-button-left"><span>We are trusted</span></AnchorLink>
    </div>
  )
}
