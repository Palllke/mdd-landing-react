import React from 'react';
import AnchorLink from 'react-anchor-link-smooth-scroll';

import HowWorkItem from './how-works-item';

import { howWorks } from '../../../data';

export default function HowWorks() {
  const howWorkItems = howWorks.map((elem) => <HowWorkItem
    key={elem.id}
    text={elem.text}
    title={elem.title}
    img={elem.img}
    imgAlt={elem.imgAlt}
  />);
  return(
    <div id="howWorks" className="howWorks">
      <div className="howWorks-wrapper">
        <h2 className="section-title"><span>How it works</span></h2>
        <div className="howWorks-item-wrapper">
          {howWorkItems}
        </div>
      </div>
      <AnchorLink offset='100' href="#evaluate-website" className="down-button down-button-left"><span>Evaluate your website</span></AnchorLink>
    </div>
  )
}
