import React from 'react';

export default function HowWorkItem({img, text, title, imgAlt}){
  return (
    <div className="howWorks-item">
      <div className="howWorks-item-anim" data-aos='img-left' />
      <img src={img} alt={imgAlt} />
      <p>{title}</p>
      <p className="reverse">{text}</p>
    </div>
  )
}
