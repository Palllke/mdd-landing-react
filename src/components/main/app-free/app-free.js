import React from 'react';
import AnchorLink from 'react-anchor-link-smooth-scroll';

import { Form } from '../../form';

export default function AppFree() {
  return(
    <section id="app-free" className="section section-app-free">
      <div className="form-wrapper">
        <h2 className="section-title section-title-right"><span>Start website refinement now</span></h2>
        <Form
          buttonText={'Start refinement'}
          classListForm={'form form-app-free flex-column'}
          classListButton={''}
        />

      </div>

      <AnchorLink offset='100' href="#faq" className="down-button"><span>FAQ</span></AnchorLink>
    </section>
  )
}
