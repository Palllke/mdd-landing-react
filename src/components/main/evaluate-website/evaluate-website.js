import React from 'react';
import { Form } from '../../form';
import AnchorLink from 'react-anchor-link-smooth-scroll';

export default function EvaluateWebsite() {
  return (
    <section id="evaluate-website" className="section section-app-free">
      <div className="form-wrapper">
        <h2 className="section-title section-title-right"><span>Evaluate your website for free</span></h2>
        <Form
          buttonText={'Evaluate My Website'}
          classListForm={'form form-app-free form-app-free--first flex-column'}
          classListButton={'button-wrapper-black'}
        />
      </div>

      <AnchorLink offset='150' href="#technology" className="down-button"><span>Technology</span></AnchorLink>
    </section>
  )
}
