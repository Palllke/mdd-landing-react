import React from 'react';

import { FormQuestion } from '../../form';

export default function Question() {
  return (
    <section id="question" className="section section-question">
      <div className="form-wrapper">
        <h2 className="section-title"><span>Do you still have a question?</span></h2>
        <FormQuestion buttonText={'Submit a question'} />
      </div>
    </section>
  )
}
