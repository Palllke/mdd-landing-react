import React from 'react';
import AnchorLink from 'react-anchor-link-smooth-scroll';

import TechnologyItem from './technology-item';

import { technology1, technology2, technology3 } from '../../../data';

export default function Technology() {
  const group1 = technology1.map((elem) => <TechnologyItem key={elem.id} img={elem.img} />);
  const group2 = technology2.map((elem) => <TechnologyItem key={elem.id} img={elem.img} />);
  const group3 = technology3.map((elem) => <TechnologyItem key={elem.id} img={elem.img} />);

  return(
    <div id="technology" className="technology">
      <div className="technology-wrapper">
        <h2 className="section-title"><span>Technology</span></h2>
        <div className="technology-item-block">
          <div className="technology-item-wrapper">
            <div className="technology-item" data-aos='go-left'>
              {group1}
            </div>
            <div className="technology-item-wrapper__anim"
                 data-aos='img-left'
                 data-aos-duration="1200"
            />
          </div>
          <div className="technology-item-wrapper">
            <div className="technology-item">
              {group2}
            </div>
            <div className="technology-item-wrapper__anim"
                 data-aos='img-left'
                 data-aos-duration="1200"
            />
          </div>
          <div className="technology-item-wrapper">
            <div className="technology-item">
              {group3}
            </div>
            <div className="technology-item-wrapper__anim"
                 data-aos='img-left'
                 data-aos-duration="1200"
            />
          </div>


        </div>
      </div>
      <AnchorLink offset='100' href="#reason" className="down-button"><span>5 reasons to choose us</span></AnchorLink>
    </div>
  )
}
