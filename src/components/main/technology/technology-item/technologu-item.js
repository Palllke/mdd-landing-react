import React from 'react';

export default function TechnologyItem({img}) {
  return (
    <div>
      <img src={img} alt="technology logo"/>
    </div>
  )
}
