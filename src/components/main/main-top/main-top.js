import React from 'react';
import AnchorLink from 'react-anchor-link-smooth-scroll';

import EvaluateFrom from '../../form/evaluate-form';

export default function MainTop() {
  return (
    <div className="main-top">
      <div className="main-top-wrapper">
        <div className="main-top-text">
          <h1 className="main-top__title" id="main-top__title">Refine your website</h1>
          <p className="main-top__subtitle">Your website should do more than look good <br /> It should work for you</p>
        </div>
        <EvaluateFrom />
      </div>

      <AnchorLink offset='150' href="#canDo" className="down-button"><span>What we can do for you</span></AnchorLink>
    </div>
  )
}
