import React from 'react';

export default function ReasonItem({text,  changeActive, index, activeNumber}) {
  let active;
  index === activeNumber ? active = true : active = false;

  return(
    <li
      className={`reason-item ${active&&'reason-item-active'}`}
      onMouseEnter={() => changeActive(index)}
    >{text}</li>
  )
}
