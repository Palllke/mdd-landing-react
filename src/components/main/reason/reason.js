import React, { useState } from 'react';
import AnchorLink from 'react-anchor-link-smooth-scroll';

import ReasonItem from './reason-item';
import { reasons } from '../../../data';

export default function Reason() {
  const [activeNumber, changeActive] = useState(1);
  const [itemLength] = useState(reasons.length);
  const reasonItems = reasons.map((elem, index) =>
    <ReasonItem
      key={elem.id}
      text={elem.text}
      changeActive={changeActive}
      index={index}
      activeNumber={activeNumber}
    />);
  return(
    <div id="reason" className="reason">
      <div className="reason-wrapper">
        <h3 className="reason-title">{ itemLength } reasons<br /> to choose us</h3>
        <div className="reason-counter"><span id="reason-counter">0{ activeNumber + 1 }</span></div>
        <ul className="reason-list">
          { reasonItems }
        </ul>
      </div>
      <AnchorLink offset='100' href="#app-free" className="down-button down-button-left"><span>Evaluate your website</span></AnchorLink>
    </div>
  )
}
