import React, { useState } from 'react';
import AnchorLink from 'react-anchor-link-smooth-scroll';

import FeedbackItem from './feedback-item';
import feedbackNext from './img/feedback-next.svg';
import feedbackPrev from './img/feedback-prev.svg';
import { feedback } from '../../../data';

export default function Feedback() {
  const [show, changeShow] = useState(1);

  function handleChange(val) {
    if (show === feedback.length -1 && val === 1)  {
      changeShow(1);
      return false;
    }
    if (show === 1 && val === -1) {
      changeShow(feedback.length -1);
      return false;
    }
    changeShow( show + val)
  }
  const feedbackItems = feedback.map((elem, index) => {
    if (index === show) {
      return <FeedbackItem key={elem.id} text={elem.text} />
    }
   return null
  });
  return (
    <div id="feedback" className="feedback">
      <div className="feedback-wrapper">
        <h2 className="section-title section-title-right"><span>Feedback</span></h2>
        <div className="flex-row">
          <div className="feedback-black"/>
          <ul className="feedback-item-wrapper">
            {feedbackItems}
          </ul>
        </div>
        <div className="feedback-buttons">
          <button
            className="feedback-prev"
            onClick={() => {handleChange(-1)}}
          >
            <img src={feedbackPrev} alt="Prev"/>
          </button>
          <button
            className="feedback-next"
            onClick={() => {handleChange(1)}}
          >
            <img src={feedbackNext} alt="Next"/>
          </button>
        </div>
      </div>
      <AnchorLink offset='100' href="#question" className="down-button"><span>Do you still have a question?</span></AnchorLink>
    </div>
  )
}
