import React from 'react';

export default function FeedbackItem({text}) {
  return(
    <li className={'feedback-item feedback-item-show'}>{text}</li>
  )
}
