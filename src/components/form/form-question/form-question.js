import React from 'react';
import FormWrapper from '../../../hoc/form-wrapper';

function FormQuestion({ submit,
                        handleUserInput,
                        name,
                        email,
                        text,
                        emailClasses,
                        span,
                        spanText,
                        textClasses,
                        blur,
                        buttonText}) {
  return (
    <div className="form-wrapper">

      <form id="form-question"
            className="form form-question flex-column align-items-center"
            onSubmit={(evt) => submit(evt, 'email', 'text')}
      >
        <div className="group-input flex-row justify-content-space-between">
          <div className="group">
            <input
              type="text"
              name="name"
              placeholder=""
              onChange={ handleUserInput }
              value={name}
              className={name.trim()=== '' ? null : 'valid'}
            />
            <span className="bar" />
            <label>Your name</label>
            <span className="error name" />
          </div>

          <div className="group">
            <input
              type="text"
              name="email"
              placeholder=""
              onChange={ handleUserInput }
              value={email}
              className={ emailClasses }
              onBlur={blur}
            />
            <span className="bar" />
            <label>Your e-mail</label>
            {span}
          </div>
        </div>

        <div className="group group-textarea">
          <input
            type="text"
            name="text"
            placeholder=""
            onChange={ handleUserInput }
            value={ text }
            className={ textClasses }
            onBlur={blur}
          />
          <span className="bar" />
          <label>Your question</label>
          {spanText}
        </div>

        <div className="button-wrapper flex-row align-items-center justify-content-center">
          <button name="submit" className="button button-white">{buttonText}</button>
        </div>
      </form>
    </div>
  )
}

export default FormWrapper(FormQuestion)
