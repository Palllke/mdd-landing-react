import React  from 'react';
import FormWrapper from '../../../hoc/form-wrapper';

function EvaluateFrom({site,
                       email,
                       handleUserInput,
                       blur,
                       emailClasses,
                       span,
                       submit}) {

  return (
    <form
      id="form-main-top"
      className="form form-main-top flex-row align-items-center"
      onSubmit={(evt) => submit(evt, 'email')}
    >
      <div className="group">
        <input
          className={site.trim()=== '' ? null : 'valid'}
          type="text"
          name="site"
          placeholder=""
          value={site}
          onChange={handleUserInput}
        />
        <span className="bar" />
        <label>http://yourwebsite</label>
        <span className="error site" />
      </div>

      <div className="group">
        <input
          type="text"
          name="email"
          placeholder=""
          value={ email }
          onBlur={ blur }
          onChange={ handleUserInput }
          className={ emailClasses }
        />
        <span className="bar" />
        <label>Your e-mail</label>
        {span}
      </div>

      <div className="button-wrapper button-wrapper--top">
        <button type="submit" name="submit" className="button button-black button-black--top">
          Evaluate My Website
        </button>
      </div>

    </form>
  )
}

export default FormWrapper(EvaluateFrom)
