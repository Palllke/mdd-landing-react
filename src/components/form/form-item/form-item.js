import React from 'react';
import FormWrapper from '../../../hoc/form-wrapper';


function Form({     buttonText,
                    name,
                    site,
                    email,
                    handleUserInput,
                    classListForm,
                    classListButton,
                    blur,
                    emailClasses,
                    span,
                    submit}) {
  return (
    <form id="form-app-free--first" className={classListForm}
      onSubmit={(evt) => submit(evt, 'email')}
    >
      <div className="group-input flex-row justify-content-space-between">
        <div className="group">
          <input
            type="text"
            name="site"
            placeholder=""
            value={site}
            onChange={ handleUserInput }
            className={site.trim()=== '' ? null : 'valid'}
          />
          <span className="bar" />
          <label>http://yourwebsite</label>
        </div>
        <div className="group">
          <input
            type="text"
            name="email"
            placeholder=""
            value={email}
            onChange={ handleUserInput }
            onBlur={blur}
            className={ emailClasses }
          />
          <span className="bar" />
          <label>Your e-mail</label>
          {span}
        </div>
      </div>
      <div className="group group-left">
        <input
          type="text"
          name="name"
          placeholder=""
          value={name}
          onChange={ handleUserInput }
          className={name.trim()=== '' ? null : 'valid'}
        />
        <span className="bar" />
        <label>Your name</label>
      </div>
      <div className={`button-wrapper flex-row align-items-center justify-content-center  ${classListButton}`}>
        <button
          type="submit"
          name="submit"
          className={`button ${classListButton === 'button-wrapper-black' ?'button-black': 'button-white'}`
          }>{buttonText}</button>
      </div>
    </form>
  )
}

export default FormWrapper(Form)
