import React, { Fragment, Component } from 'react';

import Header from './components/header';
import Main from './components/main';
import Footer from './components/footer';
import Overlay from './components/overlay';
import Cookies from './components/cookies';
import Loader from './components/loader';


class App extends Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      cookies: false
    }
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({loading: false})
    }, 500)
    if (!localStorage.getItem('cookies') || localStorage.getItem('cookies') === undefined) {
      this.setState({cookies: true})
    }
  }
  handleClose = () => {
    localStorage.setItem('cookies', 'false');
    this.setState({cookies: false})
  };

  render() {
    const { loading, cookies } = this.state;
    return(
      <Fragment>
        <Loader loading={loading}/>
        <Header />
        <Main />
        <Footer />
        <Overlay />
        <Cookies cookies={cookies} handleClose={this.handleClose}/>
      </Fragment>
    )
  }
}

export default App;
