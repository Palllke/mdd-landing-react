

window.onload = function () {
    setTimeout(function () {
        AOS.init();
    }, 1500)
};



/* Smooth scrolling to jQuery anchor */

var $page = $('html, body');

$('a[href*="#"]').click(function () {
    $page.animate({
        scrollTop: $($.attr(this, 'href')).offset().top - 75
    }, 500);
    return false;
});

//=================================================

/* Animation initialization */

AOS.init();

